
import UIKit
import Web3swift
import BigInt
import Foundation
import EthereumAddress

import ContractSwift_Library
import CryptoSwift

//https://rinkeby.etherscan.io/address/0x0c1d265726d6216edf7e7b1ed33f8a3b2b2a62a1

class ViewController: UIViewController {

    var jsonString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //readButtonClick()
        //writeButtonClick()
        
        //var msg = Contract.Abi1.Function.getUserCount.txDataString()

        //var msg = Contract.Abi1.Function.getUserAtIndex(1).txDataString()

        var msg = Contract.Abi1.Function.getUser(Address(string: "0x0C1d265726D6216edf7E7B1eD33f8a3b2b2A62A1")).txDataString()
        //let data: Data? = "test1@mobiloitte.com".data(using: .utf8) // non-nil

        //let msg = Contract.Abi1.Function.insertUser(Address(string: "0x41fC4d7fD274a40e154F7B299832b5660eD19076"),data!, 32)

       
        print("msg",msg)
        //print("msg-1 ", Web3.Utils.publicToAddressData(<#T##publicKey: Data##Data#>)

 
        
    }
    
 
    
    func readButtonClick() {
        do {
            let userDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            
            let keystoreManager = KeystoreManager.managerForPath(userDir + "/keystore")
            
            var ks: EthereumKeystoreV3?
            
            if (keystoreManager?.addresses?.count == 0) {
                
                ks = try! EthereumKeystoreV3(password: "pawan084")
                
                let keydata = try! JSONEncoder().encode(ks!.keystoreParams)
                
                FileManager.default.createFile(atPath: userDir + "/keystore"+"/key.json", contents: keydata, attributes: nil)
                
            } else {
                
                ks = keystoreManager?.walletForAddress((keystoreManager?.addresses![0])!) as! EthereumKeystoreV3
                
            }
           
            guard let sender = ks?.addresses?.first else {return}
            
            print(sender)
            var yourContractABI: String = "" //jsonString
            
            let toEthereumAddress = EthereumAddress("0x41fC4d7fD274a40e154F7B299832b5660eD19076")!
            let abiVersion: Int =  1
            
            let contract = Web3.InfuraRinkebyWeb3().contract(yourContractABI, at: toEthereumAddress, abiVersion: abiVersion)
            let method: String = "getUserCount()"
            
            //var parameters: [AnyObject] = [sender.address as AnyObject]
            var parameters: [AnyObject] = []

            let extraData: Data? = "Café".data(using: .utf8)
            
            var transactionOptions: TransactionOptions = TransactionOptions.defaultOptions
            
            transactionOptions.from = ks?.addresses?.first!
            
            transactionOptions.value = BigUInt(100)
            
            transactionOptions.from = sender
            
 
            let transaction = contract!.read(method, parameters: parameters, extraData: extraData!, transactionOptions: transactionOptions)

           // var str = String(data: (transaction?.transaction.description.data(using: .utf8))!, encoding: .utf8)
            print("transaction",transaction?.transaction.description)
            
        
        } catch {
            
            print("Unexpected error: \(error).")
            
        }
    }
    
func writeButtonClick() {
        do {
            let userDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            
            let keystoreManager = KeystoreManager.managerForPath(userDir + "/keystore")
            
            var ks: EthereumKeystoreV3?
            
            if (keystoreManager?.addresses?.count == 0) {
                
                ks = try! EthereumKeystoreV3(password: "pawan084")
                
                let keydata = try! JSONEncoder().encode(ks!.keystoreParams)
                
                FileManager.default.createFile(atPath: userDir + "/keystore"+"/key.json", contents: keydata, attributes: nil)
                
            } else {
                
                ks = keystoreManager?.walletForAddress((keystoreManager?.addresses![0])!) as! EthereumKeystoreV3
                
            }
            
            
            
            guard let sender = ks?.addresses?.first else {return}
            
            print(sender)
            var yourContractABI: String = jsonString
            
            let toEthereumAddress = EthereumAddress("0x41fC4d7fD274a40e154F7B299832b5660eD19076")!
            
            
            
            
            
            let abiVersion: Int =  2
            
            let contract = Web3.InfuraRinkebyWeb3().contract(yourContractABI, at: toEthereumAddress, abiVersion: abiVersion)
            
            
            
            let method: String = "insertUser"
            
            
            
            
            
            var parameters: [AnyObject] = [sender.address as AnyObject,"test1@mobiloitte.com" as AnyObject,BigUInt(30) as AnyObject]//userAddress
            
            //let extraData: Data = Data.
            
            
            
            let extraData: Data? = "testData".data(using: .utf8) // non-nil
            
            
            
            var transactionOptions: TransactionOptions = TransactionOptions.defaultOptions
            
            //transactionOptions.gasLimit = BigUInt(10000)
            
            transactionOptions.from = ks?.addresses?.first!
            
            transactionOptions.value = BigUInt(100)
            
            transactionOptions.from = sender
            
            let transaction = contract!.method(method, parameters: parameters, extraData: extraData!, transactionOptions: transactionOptions)
            
            print("transaction",transaction)
            
        } catch {
            print("Unexpected error: \(error).")
        }
        
    }
}
